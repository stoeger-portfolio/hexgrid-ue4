// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "HexCell.h"
#include "Hexmetrics.h"
#include "HexGridChunk.h"
#include "HexMeshComponent.h"
#include "HexGrid.generated.h"

UCLASS()
class HEXMAP_API AHexGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHexGrid();
	UPROPERTY(EditAnywhere, Category = "Hex")
	FLinearColor defaultColor = FLinearColor::Red;
	UPROPERTY(EditAnywhere, Category = "Hex")
	int chunkCountX = 4;
	UPROPERTY(EditAnywhere, Category = "Hex")
	int chunkCountZ = 3;

	UFUNCTION(BlueprintCallable)
	AHexCell* GetCellByLocation(FVector location);

	UFUNCTION(BlueprintCallable)
	AHexCell* GetCellByCoordinates(int x, int y);

	UFUNCTION(BlueprintCallable)
	void ShowCoordinates(bool visible);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	TArray<AHexCell*> cells;
	TArray<AHexGridChunk*> chunks;

	void CreateCell(int x, int y, int i);

	UFUNCTION()
	void CreateCells();
	UFUNCTION()
	void CreateChunks();
	UFUNCTION()
	void AddCellToChunk(int x, int y, AHexCell* cell);

	UPROPERTY(VisibleAnywhere, Category = "Hex")
	int cellCountX;
	UPROPERTY(VisibleAnywhere, Category = "Hex")
	int cellCountZ;
};
