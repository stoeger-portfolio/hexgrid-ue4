// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <string>
#include "Hexmetrics.h"
#include "HexCoordinates.generated.h" // this must be last include in the file

USTRUCT()
struct FHexCoordinates
{
	GENERATED_USTRUCT_BODY()

	inline int X() const
	{
		return this->x;
	}
	inline int Y() const
	{
		return -this->x - this->z;
	}

	inline int Z() const
	{
		return this->z;
	}
	inline FHexCoordinates()
	{}

	inline FHexCoordinates(const int x, const int z)
		: x(x)
		, z(z)
	{}

	inline static FHexCoordinates FromOffsetCoordinates(const int x, const int z)
	{
		return FHexCoordinates(x - z / 2, z);
	}

	static FHexCoordinates FromPosition(const FVector position)
	{
		float x = position.X / (FHexMetrics::innerRadius * 2.0f);
		float z = -x;
		float offset = position.Y / (FHexMetrics::outerRadius * 3.0f);
		x -= offset;
		z -= offset;

		int iX = round(x);
		int iY = round(-x - z);
		int iZ = round(z);

		if (iX + iY + iZ != 0)
		{
			float dX = abs(x - iX);
			float dY = abs(-x - z - iY);
			float dZ = abs(z - iZ);

			if (dX > dZ && dX > dY)
			{
				iX = -iY - iZ;
			}
			else if (dY > dZ) {
				iY = -iX - iZ;
			}
		}

		return FHexCoordinates(iX, iY);
	}

	inline FString ToString()
	{
		FString result = "";
		result.AppendInt(this->x);
		result.Append(", ");
		result.AppendInt(this->Y());
		result.Append(", ");
		result.AppendInt(this->z);
		return result;
	}

	inline FString ToStringOnSeparateLines()
	{
		FString result = "";
		result.AppendInt(this->x);
		result.Append("<br>");
		result.AppendInt(this->Y());
		result.Append("<br>");
		result.AppendInt(this->z);
		return result;
	}

private:
	int x;
	int z;
};