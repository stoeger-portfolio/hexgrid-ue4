// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ConstructorHelpers.h"
#include "ProceduralMeshComponent.h"
#include "HexCell.h"
#include "Hexmetrics.h"
#include "HexMesh.generated.h"

UCLASS()
class HEXMAP_API AHexMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHexMesh();
	UProceduralMeshComponent* MeshComponent;
	TArray<FVector> vertices;
	TArray<FVector> normals; 
	TArray<FVector2D> UV0;
	TArray<int32> triangles;
	TArray<FProcMeshTangent> tangents;
	TArray<FLinearColor> vertexColors;
	void Triangulate(TArray<AHexCell*> cells);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	void Triangulate(AHexCell* cell);
	void AddTriangle(FVector v1, FVector v2, FVector v3);
};
