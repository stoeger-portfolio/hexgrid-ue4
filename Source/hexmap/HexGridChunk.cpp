// Fill out your copyright notice in the Description page of Project Settings.


#include "HexGridChunk.h"
#include "HexCell.h"

// Sets default values
AHexGridChunk::AHexGridChunk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent")));

	hexMesh = CreateDefaultSubobject<UHexMeshComponent>(TEXT("HexMesh"));
	cells = TArray<AHexCell*>();
	cells.SetNum(FHexMetrics::chunkSizeX * FHexMetrics::chunkSizeZ);
}

// Called when the game starts or when spawned
void AHexGridChunk::BeginPlay()
{
	Super::BeginPlay();
}

void AHexGridChunk::Refresh()
{
	SetActorTickEnabled(true);
}

// Called every frame
void AHexGridChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	hexMesh->Triangulate(cells);
	SetActorTickEnabled(false);
}

void AHexGridChunk::AddCell(int index, AHexCell* cell)
{
	cell->chunk = this;
	cells[index] = cell;
	cell->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

void AHexGridChunk::ShowCoordinates(bool visible)
{
	for (int i = 0; i < cells.Num(); ++i)
	{
		cells[i]->ShowCoordinates(visible);
	}
}