// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "HexCoordinates.h"
#include "Enum/HexDirection.h"
#include "Enum/HexEdgeType.h"
#include "Runtime/Engine/Classes/Engine/TextRenderActor.h"
#include "Runtime/Engine/Classes/Components/TextRenderComponent.h"
#include "HexCell.generated.h"

class AHexGridChunk;

UCLASS()
class HEXMAP_API AHexCell : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHexCell();

	UPROPERTY(VisibleAnywhere, Category = "Hex")
	FHexCoordinates coordinates;
	UPROPERTY(VisibleAnywhere, Category = "Hex")
	ATextRenderActor* text;

	UPROPERTY()
	AHexGridChunk* chunk;

	UFUNCTION(BlueprintCallable)
	void ShowCoordinates(bool visible)
	{
		text->SetActorHiddenInGame(!visible);
	}

	UFUNCTION(BlueprintCallable)
	inline int GetCoordinateX()
	{
		return coordinates.X();
	}

	UFUNCTION(BlueprintCallable)
	inline int GetCoordinateY()
	{
		return coordinates.Y();
	}

	UFUNCTION(BlueprintCallable)
	inline int GetCoordinateZ()
	{
		return coordinates.Z();
	}

	UFUNCTION(BlueprintCallable)
	void SetElevation(int value);	

	UFUNCTION(BlueprintCallable)
	void SetColor(FLinearColor value);

	UFUNCTION(BlueprintCallable)
	inline FLinearColor GetColor() const
	{
		return color;
	}
	UFUNCTION(BlueprintCallable)
	inline int GetElevation() const
	{
		return elevation;
	}
	UFUNCTION()
	inline EHexEdgeType GetEdgeType(const EHexDirection direction) const
	{
		return HexEdgeType::GetEdgeType(elevation, neighbors[(int)direction]->elevation);
	}
	UFUNCTION()
	inline AHexCell* GetNeighbor(const EHexDirection direction) const
	{
		return neighbors[(int)direction];
	}
	UFUNCTION()
	inline EHexEdgeType GetEdgeTypeByOtherCell(const AHexCell *otherCell) const
	{
		return HexEdgeType::GetEdgeType(elevation, otherCell->GetElevation());
	}
	UFUNCTION()
	inline bool GetHasIncommingRiver()
	{
		return hasIncomingRiver;
	}
	UFUNCTION()
	inline bool GetHasOutgoingRiver()
	{
		return hasOutgoingRiver;
	}
	UFUNCTION()
	inline EHexDirection GetIncomingRiverDirection()
	{
		return incommingRiver;
	}
	UFUNCTION()
	inline EHexDirection GetOutgoingRiverDirection()
	{
		return outgoingRiver;
	}
	UFUNCTION()
	inline bool HasRiver()
	{
		return hasIncomingRiver || hasOutgoingRiver;
	}
	UFUNCTION()
	inline bool HasRiverBeginOrEnd()
	{
		return hasIncomingRiver != hasOutgoingRiver;
	}
	UFUNCTION()
	inline bool HasRiverThroughEdge(EHexDirection direction)
	{
		return hasIncomingRiver && incommingRiver == direction || hasOutgoingRiver && outgoingRiver == direction;
	}

	inline void SetIncomingRiverDirection(EHexDirection value)
	{
		incommingRiver = value;
	}
	
	inline void RemoveRiver()
	{
		RemoveIncommingRiver();
		RemoveOutgoingRiver();
	}

	inline void SetNeighbor(const EHexDirection direction, AHexCell* cell)
	{
		neighbors[(int)direction] = cell;
		cell->neighbors[(int)HexDirection::Opposite(direction)] = this;
	}

	inline void SetHasIncomingRiver(bool value)
	{
		hasIncomingRiver = value;
	}

	inline void SetHasOutgoingRiver(bool value)
	{
		hasOutgoingRiver = value;
	}

	inline void RefreshSelf();
	void SetOutGoingRiver(EHexDirection direction);
	inline void RemoveIncommingRiver();
	inline void RemoveOutgoingRiver();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere, Category = "Hex")
	TArray<AHexCell*> neighbors;	
	
	UPROPERTY(VisibleAnywhere, Category = "Hex")
	int elevation = INT_MIN;

	UPROPERTY(VisibleAnywhere, Category = "Hex")
	FLinearColor color;

	UPROPERTY(VisibleAnywhere, Category = "Hex")
	bool hasIncomingRiver;
	UPROPERTY(VisibleAnywhere, Category = "Hex")
	bool hasOutgoingRiver;

	UPROPERTY()
	EHexDirection incommingRiver;
	UPROPERTY()
	EHexDirection outgoingRiver;

	UFUNCTION()
	void Refresh();
};
