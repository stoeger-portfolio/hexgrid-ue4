// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enum/HexDirection.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Hexmetrics.generated.h" // this must be last include in the file

USTRUCT()
struct FHexMetrics
{
	GENERATED_USTRUCT_BODY()

	static const float outerRadius;
	static const float innerRadius;
	static const float solidFactor;
	static const float blendFactor;

	static const int chunkSizeX;
	static const int chunkSizeZ;

	static const float elevationStep;

	static const int terracesPerSlope;
	static const int terraceSteps;
	static const float horizontalTerraceStepSize;
	static const float verticalTerraceStepSize;

	static const TArray<FVector> corner;

	static const UTexture2D* noiseSource;

	static inline FVector GetFirstCorner(EHexDirection direction)
	{
		return corner[(int)direction];
	}
	static inline FVector GetSecondCorner(EHexDirection direction)
	{
		return corner[(int)direction + 1];
	}
	static inline FVector GetFirstSolidCorner(EHexDirection direction)
	{
		return corner[(int)direction] * solidFactor;
	}
	static inline FVector GetSecondsSolidCorner(EHexDirection direction)
	{
		return corner[(int)direction + 1] * solidFactor;
	}
	static inline FVector GetBridge(EHexDirection direction)
	{
		return (corner[(int)direction] + corner[(int)direction + 1]) * blendFactor;
	}

	static inline FVector TerraceLerp(FVector a, FVector b, int step)
	{
		float h = step * FHexMetrics::horizontalTerraceStepSize;
		a.X += (b.X - a.X) * h;
		a.Y += (b.Y - a.Y) * h;
		float v = ((step + 1) / 2) * FHexMetrics::verticalTerraceStepSize;
		a.Z += (b.Z - a.Z) * v;
		return a;
	}

	static inline FLinearColor TerraceLerp(FLinearColor a, FLinearColor b, int step)
	{
		float h = step * FHexMetrics::horizontalTerraceStepSize;
		return FLinearColor::LerpUsingHSV(a, b, h);
	}
};