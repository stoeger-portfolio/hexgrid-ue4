#include "Hexmetrics.h"

const TArray<FVector> FHexMetrics::corner = {
		FVector(0.0f, FHexMetrics::outerRadius, 0.0f),
		FVector(innerRadius, 0.5f * outerRadius, 0.0f),
		FVector(innerRadius, -0.5f * outerRadius, 0.0f),
		FVector(0.0f, -outerRadius, 0.0f),
		FVector(-innerRadius, -0.5f * outerRadius, 0.0f),
		FVector(-innerRadius, 0.5f * outerRadius, 0.0f),
		FVector(0.0f, FHexMetrics::outerRadius, 0.0f)
};

const float FHexMetrics::outerRadius = 10.0f;
const float FHexMetrics::innerRadius = FHexMetrics::outerRadius * 0.866025404f;
const float FHexMetrics::solidFactor = 0.75f;
const float FHexMetrics::blendFactor = 1.0f - FHexMetrics::solidFactor;

const float FHexMetrics::elevationStep = 5.0f;

const int FHexMetrics::terracesPerSlope = 2;
const int FHexMetrics::terraceSteps = terracesPerSlope * 2 + 1;
const float FHexMetrics::verticalTerraceStepSize = 1.f / (FHexMetrics::terracesPerSlope + 1);
const float FHexMetrics::horizontalTerraceStepSize = 1.0f / FHexMetrics::terraceSteps;

const int FHexMetrics::chunkSizeX = 5;
const int FHexMetrics::chunkSizeZ = 5;