// Fill out your copyright notice in the Description page of Project Settings.


#include "HexMeshComponent.h"

static TArray<FVector> vertices;
static TArray<FVector> normals;
static TArray<FVector2D> UV0;
static TArray<int32> triangles;
static TArray<FProcMeshTangent> tangents;
static TArray<FLinearColor> vertexColors;

// Sets default values for this component's properties
UHexMeshComponent::UHexMeshComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("MeshComponent"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialOb(TEXT("Material'/Game/M_HexMesh.M_HexMesh'"));
	material = MaterialOb.Object;
	MeshComponent->SetMaterial(0, material);
	// ...
}

// Called when the game starts
void UHexMeshComponent::BeginPlay()
{
	Super::BeginPlay();

	colors = TArray<FColor>();
	// ...
	
}


// Called every frame
void UHexMeshComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHexMeshComponent::Triangulate(TArray<AHexCell*> cells)
{
	vertices.Empty();
	triangles.Empty();
	normals.Empty();
	UV0.Empty();
	vertexColors.Empty();
	tangents.Empty();
	colors.Empty();
	for (int i = 0; i < cells.Num(); ++i)
	{
		if (cells[i] != nullptr) {
			Triangulate(cells[i]);
		}
	}
	MeshComponent->CreateMeshSection_LinearColor(0, vertices, triangles, normals, UV0, vertexColors, tangents, true);
	MeshComponent->ContainsPhysicsTriMeshData(true);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	MeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Block);
}

void UHexMeshComponent::Triangulate(const AHexCell* cell)
{
	for (EHexDirection d : HexDirection::all_directions)
	{
		Triangulate(d, cell);
	}
}
void UHexMeshComponent::Triangulate(EHexDirection direction, const AHexCell* cell)
{
	FVector center = cell->GetActorLocation();
	FVector v1 = center + FHexMetrics::GetFirstSolidCorner(direction);
	FVector v2 = center + FHexMetrics::GetSecondsSolidCorner(direction);

	AddTriangle(center, v1, v2, cell->GetColor());

	if (direction <= EHexDirection::E_SE) {
		TriangulateConnection(direction, cell, v1, v2);
	}
}

void UHexMeshComponent::TriangulateConnection(EHexDirection direction, const AHexCell* cell, FVector v1, FVector v2)
{
	const AHexCell* neighbor = cell->GetNeighbor(direction);
	const AHexCell* nextNeighbor = cell->GetNeighbor(HexDirection::Next(direction));
	if (neighbor == nullptr)
	{
		return;
	}

	FVector bridge = FHexMetrics::GetBridge(direction);
	FVector v3 = v1 + bridge;
	FVector v4 = v2 + bridge;

	int neighborElevation = neighbor->GetElevation();
	v3.Z = v4.Z = neighborElevation * FHexMetrics::elevationStep;

	if (direction <= EHexDirection::E_E && nextNeighbor != nullptr)
	{
		FVector v5 = v2 + FHexMetrics::GetBridge(HexDirection::Next(direction));

		int nextNeighborElevation = nextNeighbor->GetElevation();
		v5.Z = nextNeighborElevation * FHexMetrics::elevationStep;
		
		int cellElevation = cell->GetElevation();
		if (cellElevation <= neighborElevation)
		{
			if (cellElevation <= nextNeighborElevation)
			{
				TriangulateCorner(v2, v4, v5, cell, neighbor, nextNeighbor);
			}
			else {
				TriangulateCorner(v5, v2, v4, nextNeighbor, cell, neighbor);
			}
		}
		else if (neighborElevation <= nextNeighborElevation) {
			TriangulateCorner(v4, v5, v2, neighbor, nextNeighbor, cell);
		}
		else {
			TriangulateCorner(v5, v2, v4, nextNeighbor, cell, neighbor);
		}
	}

	if (cell->GetEdgeType(direction) == EHexEdgeType::E_Slope)
	{
		TriangulateEdgeTerraces(v1, v2, cell, v3, v4, neighbor);
	}
	else {
		AddQuad(v1, v2, v3, v4, cell->GetColor(), neighbor->GetColor());
	}
}

void UHexMeshComponent::TriangulateCorner(FVector bottom, FVector left, FVector right, const AHexCell* bottomCell, const AHexCell* leftCell, const AHexCell* rightCell)
{
	EHexEdgeType leftEdgeType = bottomCell->GetEdgeTypeByOtherCell(leftCell);
	EHexEdgeType rightEdgeType = bottomCell->GetEdgeTypeByOtherCell(rightCell);

	if (leftEdgeType == EHexEdgeType::E_Slope)
	{
		if (rightEdgeType == EHexEdgeType::E_Slope)
		{
			TriangulateCornerTerraces(bottom, left, right, bottomCell, leftCell, rightCell);
		}
		else if (rightEdgeType == EHexEdgeType::E_Flat)
		{
			TriangulateCornerTerraces(left, right, bottom, leftCell, rightCell, bottomCell);
		}
		else {
			TriangulateCornerTerracesCliff(bottom, left, right, bottomCell, leftCell, rightCell);
		}
	}
	else if (rightEdgeType == EHexEdgeType::E_Slope)
	{
		if (leftEdgeType == EHexEdgeType::E_Flat)
		{
			TriangulateCornerTerraces(right, bottom, left, rightCell, leftCell, bottomCell);
		}
		else {
			TriangulateCornerCliffTerraces(bottom, left, right, bottomCell, leftCell, rightCell);
		}
	}
	else if (leftCell->GetEdgeTypeByOtherCell(rightCell) == EHexEdgeType::E_Slope)
	{
		if (leftCell->GetElevation() < rightCell->GetElevation())
		{
			TriangulateCornerCliffTerraces(right, bottom, left, rightCell, bottomCell, leftCell);
		}
		else {
			TriangulateCornerTerracesCliff(left, right, bottom, leftCell, rightCell, bottomCell);
		}
	}
	else {
		AddTriangle(bottom, left, right, bottomCell->GetColor(), leftCell->GetColor(), rightCell->GetColor());
	}
}

void UHexMeshComponent::TriangulateCornerTerraces(FVector begin, FVector left, FVector right, const AHexCell* beginCell, const AHexCell* leftCell, const AHexCell* rightCell)
{
	FVector v3 = FHexMetrics::TerraceLerp(begin, left, 1);
	FVector v4 = FHexMetrics::TerraceLerp(begin, right, 1);
	FLinearColor c3 = FHexMetrics::TerraceLerp(beginCell->GetColor(), leftCell->GetColor(), 1);
	FLinearColor c4 = FHexMetrics::TerraceLerp(beginCell->GetColor(), rightCell->GetColor(), 1);

	AddTriangle(begin, v3, v4, beginCell->GetColor(), c3, c4);

	for (int i = 2; i < FHexMetrics::terraceSteps; ++i)
	{
		FVector v1 = v3;
		FVector v2 = v4;
		FLinearColor c1 = c3;
		FLinearColor c2 = c4;

		v3 = FHexMetrics::TerraceLerp(begin, left, i);
		v4 = FHexMetrics::TerraceLerp(begin, right, i);
		c3 = FHexMetrics::TerraceLerp(beginCell->GetColor(), leftCell->GetColor(), i);
		c4 = FHexMetrics::TerraceLerp(beginCell->GetColor(), rightCell->GetColor(), i);
		AddQuad(v1, v2, v3, v4, c1, c2, c3, c4);
	}

	AddQuad(v3, v4, left, right, c3, c4, leftCell->GetColor(), rightCell->GetColor());
}

void UHexMeshComponent::TriangulateCornerTerracesCliff(FVector begin, FVector left, FVector right, const AHexCell* beginCell, const AHexCell* leftCell, const AHexCell* rightCell)
{
	float b = 1.0f / (rightCell->GetElevation() - beginCell->GetElevation());
	if (b < 0)
	{
		b = -b;
	}
	FVector boundary = FMath::Lerp(begin, right, b);
	FLinearColor boundaryColor = FLinearColor::LerpUsingHSV(beginCell->GetColor(), rightCell->GetColor(), b);

	TriangulateBoundaryTriangle(begin, left, boundary, beginCell, leftCell, boundaryColor);

	if (leftCell->GetEdgeTypeByOtherCell(rightCell) == EHexEdgeType::E_Slope)
	{
		TriangulateBoundaryTriangle(left, right, boundary, leftCell, rightCell, boundaryColor);
	}
	else {
		AddTriangle(left, right, boundary, leftCell->GetColor(), rightCell->GetColor(), boundaryColor);
	}
}

void UHexMeshComponent::TriangulateCornerCliffTerraces(FVector begin, FVector left, FVector right, const AHexCell* beginCell, const AHexCell* leftCell, const AHexCell* rightCell)
{
	float b = 1.0f / (rightCell->GetElevation() - beginCell->GetElevation());
	if (b < 0)
	{
		b = -b;
	}
	FVector boundary = FMath::Lerp(begin, left, b);
	FLinearColor boundaryColor = FLinearColor::LerpUsingHSV(beginCell->GetColor(), leftCell->GetColor(), b);

	TriangulateBoundaryTriangle(right, begin, boundary, rightCell, beginCell, boundaryColor);

	if (leftCell->GetEdgeTypeByOtherCell(rightCell) == EHexEdgeType::E_Slope)
	{
		TriangulateBoundaryTriangle(left, right, boundary, leftCell, rightCell, boundaryColor);
	}
	else {
		AddTriangle(left, right, boundary, leftCell->GetColor(), rightCell->GetColor(), boundaryColor);
	}
}

void UHexMeshComponent::TriangulateBoundaryTriangle(FVector begin, FVector left, FVector boundary, const AHexCell *beginCell, const AHexCell *leftCell, FLinearColor boundaryColor)
{
	FVector v2 = FHexMetrics::TerraceLerp(begin, left, 1);
	FLinearColor c2 = FHexMetrics::TerraceLerp(beginCell->GetColor(), leftCell->GetColor(), 1);

	AddTriangle(begin, v2, boundary, beginCell->GetColor(), c2, boundaryColor);

	for (int i = 2; i < FHexMetrics::terraceSteps; ++i)
	{
		FVector v1 = v2;
		FLinearColor c1 = c2;
		v2 = FHexMetrics::TerraceLerp(begin, left, i);
		c2 = FHexMetrics::TerraceLerp(beginCell->GetColor(), leftCell->GetColor(), i);
		AddTriangle(v1, v2, boundary, c1, c2, boundaryColor);
	}

	AddTriangle(v2, left, boundary, c2, leftCell->GetColor(), boundaryColor);
}

void UHexMeshComponent::TriangulateEdgeTerraces(FVector beginLeft, FVector beginRight, const AHexCell *beginCell, FVector endLeft, FVector endRight, const AHexCell *endCell)
{
	FVector v3 = FHexMetrics::TerraceLerp(beginLeft, endLeft, 1);
	FVector v4 = FHexMetrics::TerraceLerp(beginRight, endRight, 1);
	FLinearColor c2 = FHexMetrics::TerraceLerp(beginCell->GetColor(), endCell->GetColor(), 1);

	AddQuad(beginLeft, beginRight, v3, v4, beginCell->GetColor(), c2);

	for (int i = 2; i < FHexMetrics::terraceSteps; ++i)
	{
		FVector v1 = v3;
		FVector v2 = v4;
		FLinearColor c1 = c2;
		v3 = FHexMetrics::TerraceLerp(beginLeft, endLeft, i);
		v4 = FHexMetrics::TerraceLerp(beginRight, endRight, i);
		c2 = FHexMetrics::TerraceLerp(beginCell->GetColor(), endCell->GetColor(), i);
		AddQuad(v1, v2, v3, v4, c1, c2);
	}

	AddQuad(v3, v4, endLeft, endRight, c2, endCell->GetColor());
}

void UHexMeshComponent::AddQuad(FVector v1, FVector v2, FVector v3, FVector v4, FLinearColor c1, FLinearColor c2)
{
	int vertexIndex = vertices.Num();
	vertices.Add(v1);
	vertices.Add(v2);
	vertices.Add(v3);
	vertices.Add(v4);

	triangles.Add(vertexIndex);
	triangles.Add(vertexIndex + 2);
	triangles.Add(vertexIndex + 1);
	triangles.Add(vertexIndex + 1);
	triangles.Add(vertexIndex + 2);
	triangles.Add(vertexIndex + 3);

	normals.Add(FVector(0, 0, 1));
	normals.Add(FVector(0, 0, 1));
	normals.Add(FVector(0, 0, 1));

	UV0.Add(FVector2D(0, 0));
	UV0.Add(FVector2D(10, 0));
	UV0.Add(FVector2D(0, 10));

	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));

	vertexColors.Add(c1);
	vertexColors.Add(c1);
	vertexColors.Add(c2);
	vertexColors.Add(c2);
}

void UHexMeshComponent::AddQuad(FVector v1, FVector v2, FVector v3, FVector v4, FLinearColor c1, FLinearColor c2, FLinearColor c3, FLinearColor c4)
{
	AddQuadMeta(v1, v2, v3, v4);

	vertexColors.Add(c1);
	vertexColors.Add(c2);
	vertexColors.Add(c3);
	vertexColors.Add(c4);
}

void UHexMeshComponent::AddTriangle(FVector v1, FVector v2, FVector v3, FLinearColor color)
{
	AddTriangleMeta(v1, v2, v3);

	vertexColors.Add(color);
	vertexColors.Add(color);
	vertexColors.Add(color);
}

void UHexMeshComponent::AddQuadMeta(FVector v1, FVector v2, FVector v3, FVector v4)
{
	int vertexIndex = vertices.Num();
	vertices.Add(v1);
	vertices.Add(v2);
	vertices.Add(v3);
	vertices.Add(v4);

	triangles.Add(vertexIndex);
	triangles.Add(vertexIndex + 2);
	triangles.Add(vertexIndex + 1);
	triangles.Add(vertexIndex + 1);
	triangles.Add(vertexIndex + 2);
	triangles.Add(vertexIndex + 3);

	normals.Add(FVector(0, 0, 1));
	normals.Add(FVector(0, 0, 1));
	normals.Add(FVector(0, 0, 1));

	UV0.Add(FVector2D(0, 0));
	UV0.Add(FVector2D(10, 0));
	UV0.Add(FVector2D(0, 10));

	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
}

void UHexMeshComponent::AddTriangle(FVector v1, FVector v2, FVector v3, FLinearColor c1, FLinearColor c2, FLinearColor c3)
{
	AddTriangleMeta(v1, v2, v3);

	vertexColors.Add(c1);
	vertexColors.Add(c2);
	vertexColors.Add(c3);
}

void UHexMeshComponent::AddTriangleMeta(FVector v1, FVector v2, FVector v3)
{
	int vertexIndex = vertices.Num();
	vertices.Add(v1);
	vertices.Add(v2);
	vertices.Add(v3);

	triangles.Add(vertexIndex);
	triangles.Add(vertexIndex + 1);
	triangles.Add(vertexIndex + 2);

	normals.Add(FVector(0, 0, 1));
	normals.Add(FVector(0, 0, 1));
	normals.Add(FVector(0, 0, 1));

	UV0.Add(FVector2D(0, 0));
	UV0.Add(FVector2D(10, 0));
	UV0.Add(FVector2D(0, 10));

	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
}