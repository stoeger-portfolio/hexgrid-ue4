// Fill out your copyright notice in the Description page of Project Settings.

#include "HexMesh.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AHexMesh::AHexMesh()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UProceduralMeshComponent >(TEXT("Hex Mesh"));
	RootComponent = MeshComponent;

	MeshComponent->bUseAsyncCooking = true;

// 	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Plane.Plane'"));
// 
// 	if (MeshAsset.Succeeded())
// 	{
// 		MeshComponent->SetStaticMesh(MeshAsset.Object);
// 		MeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
// 		MeshComponent->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
// 	}
}

// Called when the game starts or when spawned
void AHexMesh::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHexMesh::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHexMesh::Triangulate(TArray<AHexCell*> cells)
{
	this->vertices.Empty();
	this->triangles.Empty();
	for (int i = 0; i < cells.Num(); ++i)
	{
		Triangulate(cells[i]);
	}
	//MeshComponent->CreateMeshSection_LinearColor(0, vertices, triangles, normals, UV0, vertexColors, tangents, true);
	//MeshComponent->ContainsPhysicsTriMeshData(true);
}

void AHexMesh::Triangulate(AHexCell* cell)
{
	FVector center = cell->GetActorLocation();
	AddTriangle(center, center + FHexMetrics::corner[0], center + FHexMetrics::corner[1]);
}

void AHexMesh::AddTriangle(FVector v1, FVector v2, FVector v3)
{
	int vertexIndex = this->vertices.Num();
	this->vertices.Add(v1);
	this->vertices.Add(v2);
	this->vertices.Add(v3);

	this->triangles.Add(vertexIndex);
	this->triangles.Add(vertexIndex + 1);
	this->triangles.Add(vertexIndex + 2);

	this->normals.Add(FVector(1, 0, 0));
	this->normals.Add(FVector(1, 0, 0));
	this->normals.Add(FVector(1, 0, 0));

	this->UV0.Add(FVector2D(0, 0));
	this->UV0.Add(FVector2D(10, 0));
	this->UV0.Add(FVector2D(0, 10));

	this->tangents.Add(FProcMeshTangent(0, 1, 0));
	this->tangents.Add(FProcMeshTangent(0, 1, 0));
	this->tangents.Add(FProcMeshTangent(0, 1, 0));

	this->vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	this->vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	this->vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
}