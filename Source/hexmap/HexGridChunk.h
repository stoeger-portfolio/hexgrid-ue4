// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HexMeshComponent.h"
#include "HexGridChunk.generated.h"

class AHexCell;

UCLASS()
class HEXMAP_API AHexGridChunk : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHexGridChunk();
	UFUNCTION()
	void AddCell(int index, AHexCell* cell);

	UFUNCTION(BlueprintCallable)
	void Refresh();

	UFUNCTION(BlueprintCallable)
	void ShowCoordinates(bool visible);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	TArray<AHexCell*> cells;
	UHexMeshComponent* hexMesh;
};
