// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HexmapGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HEXMAP_API AhexmapGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
