// Fill out your copyright notice in the Description page of Project Settings.

#include "HexCell.h"
#include "HexGridChunk.h"

// Sets default values
AHexCell::AHexCell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent")));

	neighbors.SetNum(6);
}

// Called when the game starts or when spawned
void AHexCell::BeginPlay()
{
	Super::BeginPlay();

	FVector Location(0.0f, 0.0f, -1.0f);
	FRotator Rotation(-90.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;

	text = GetWorld()->SpawnActor<ATextRenderActor>(Location, Rotation, SpawnInfo);
	text->GetTextRender()->SetXScale(0.125);
	text->GetTextRender()->SetYScale(0.125);
	text->GetTextRender()->SetHorizontalAlignment(EHTA_Center);
	text->GetTextRender()->VerticalAlignment = EVerticalTextAligment::EVRTA_TextCenter;
	text->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	ShowCoordinates(false);
}

// Called every frame
void AHexCell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHexCell::SetElevation(int value)
{
	if (elevation == value)
	{
		return;
	}
	elevation = value;
	FVector position = GetActorLocation();
	position.Z = elevation * FHexMetrics::elevationStep;
	SetActorLocation(position);

	if (hasOutgoingRiver && elevation < GetNeighbor(outgoingRiver)->GetElevation())
	{
		RemoveOutgoingRiver();
	}
	if (hasIncomingRiver && elevation > GetNeighbor(incommingRiver)->GetElevation())
	{
		RemoveIncommingRiver();
	}

	Refresh();
}

void AHexCell::SetColor(FLinearColor value)
{
	if (color == value)
	{
		return;
	}

	color = value;
	Refresh();
}

void AHexCell::RefreshSelf()
{
	chunk->Refresh();
}

void AHexCell::Refresh()
{
	text->GetTextRender()->SetText(coordinates.ToStringOnSeparateLines());
	if (chunk != nullptr) {
		chunk->Refresh();
		for (int i = 0; i < neighbors.Num(); ++i)
		{
			AHexCell* neighbor = neighbors[i];
			if (neighbor != nullptr && neighbor->chunk != chunk)
			{
				neighbor->chunk->Refresh();
			}
		}
	}
}

void AHexCell::SetOutGoingRiver(EHexDirection direction)
{
	if (hasOutgoingRiver && outgoingRiver == direction)
	{
		return;
	}

	AHexCell* neighbor = GetNeighbor(direction);
	if (neighbor == nullptr || elevation < neighbor->elevation)
	{
		return;
	}
	RemoveOutgoingRiver();
	if (hasIncomingRiver && incommingRiver == direction)
	{
		RemoveIncommingRiver();
	}

	hasOutgoingRiver = true;
	outgoingRiver = direction;
	RefreshSelf();

	neighbor->RemoveIncommingRiver();
	neighbor->SetHasIncomingRiver(true);
	neighbor->SetIncomingRiverDirection(HexDirection::Opposite(direction));
}

void AHexCell::RemoveIncommingRiver()
{
	if (!hasIncomingRiver)
	{
		return;
	}
	hasIncomingRiver = false;
	RefreshSelf();

	AHexCell* neighbor = GetNeighbor(outgoingRiver);
	neighbor->SetHasOutgoingRiver(false);
	neighbor->RefreshSelf();
}

void AHexCell::RemoveOutgoingRiver()
{
	if (!hasOutgoingRiver)
	{
		return;
	}
	hasOutgoingRiver = false;
	RefreshSelf();

	AHexCell* neighbor = GetNeighbor(outgoingRiver);
	neighbor->SetHasIncomingRiver(false);
	neighbor->RefreshSelf();
}