// Fill out your copyright notice in the Description page of Project Settings.

#include "HexGrid.h"
#include "DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"

// Sets default values
AHexGrid::AHexGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent")));
}

// Called when the game starts or when spawned
void AHexGrid::BeginPlay()
{
	Super::BeginPlay();

	cells = TArray<AHexCell*>();

	cellCountX = chunkCountX * FHexMetrics::chunkSizeX;
	cellCountZ = chunkCountZ * FHexMetrics::chunkSizeZ;
	CreateChunks();
	CreateCells();
}

AHexCell* AHexGrid::GetCellByLocation(FVector location)
{
	FHexCoordinates clickedCell = FHexCoordinates::FromPosition(location);
	UE_LOG(LogTemp, Warning, TEXT("CLICKED ME IN GRID: %d, %d"), clickedCell.X(), clickedCell.Y());

	int index = clickedCell.X() + clickedCell.Z() * cellCountX + clickedCell.Z() / 2;

	return cells[index];
}

AHexCell* AHexGrid::GetCellByCoordinates(int x, int z)
{
	if (z < 0 || z >= cellCountZ)
	{
		return nullptr;
	}
	int localX = x + z / 2;
	if (localX < 0 || localX >= cellCountX)
	{
		return nullptr;
	}
	return cells[localX + z * cellCountX];
}

void AHexGrid::CreateChunks()
{
	chunks.SetNum(chunkCountX * chunkCountZ);
	for (size_t i = 0, z = 0; z < chunkCountZ; ++z)
	{
		for (size_t x = 0; x < chunkCountX; ++x)
		{
			FVector Location(0.0f, 0.0f, 0.0f);
			FRotator Rotation(0.0f, 0.0f, 0.0f);
			FActorSpawnParameters SpawnInfo;
			AHexGridChunk* chunk = GetWorld()->SpawnActor<AHexGridChunk>(Location, Rotation, SpawnInfo);
			chunk->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
			chunks[i++] = chunk;
		}
	}
}

void AHexGrid::CreateCells()
{
	cells.SetNum(cellCountX * cellCountZ);
	for (size_t i = 0, z = 0; z < cellCountZ; ++z)
	{
		for (size_t x = 0; x < cellCountX; ++x)
		{
			CreateCell(x, z, i++);
		}
	}
}

// Called every frame
void AHexGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHexGrid::CreateCell(int x, int z, int i)
{
	FVector Location((x + z * 0.5f - z / 2) * (FHexMetrics::innerRadius * 2.0f), z * (FHexMetrics::outerRadius * 1.5f), 0.0f);
	FRotator Rotation(0.0f, 0.0f, 180.0f);
	FActorSpawnParameters SpawnInfo;

	AHexCell* cell = GetWorld()->SpawnActor<AHexCell>(Location, Rotation, SpawnInfo);
	cell->coordinates = FHexCoordinates::FromOffsetCoordinates(x, z);
	cell->SetColor(defaultColor);

	if (x > 0) {
		cell->SetNeighbor(EHexDirection::E_W, cells[i - 1]);
	}
	if (z > 0) {
		if ((z & 1) == 0) {
			cell->SetNeighbor(EHexDirection::E_SE, cells[i - cellCountX]);
			if (x > 0) {
				cell->SetNeighbor(EHexDirection::E_SW, cells[i - cellCountX - 1]);
			}
		}
		else {
			cell->SetNeighbor(EHexDirection::E_SW, cells[i - cellCountX]);
			if (x < cellCountX - 1) {
				cell->SetNeighbor(EHexDirection::E_SE, cells[i - cellCountX + 1]);
			}
		}
	}

	AddCellToChunk(x, z, cell);
	cell->SetElevation(0);
	cells[i] = cell;
}

void AHexGrid::AddCellToChunk(int x, int z, AHexCell* cell)
{
	int chunkX = x / FHexMetrics::chunkSizeX;
	int chunkZ = z / FHexMetrics::chunkSizeZ;
	AHexGridChunk *chunk = chunks[chunkX + chunkZ * chunkCountX];

	int localX = x - chunkX * FHexMetrics::chunkSizeX;
	int localZ = z - chunkZ * FHexMetrics::chunkSizeZ;
	chunk->AddCell(localX + localZ * FHexMetrics::chunkSizeX, cell);
}

void AHexGrid::ShowCoordinates(bool visible)
{
	for (int i = 0; i < chunks.Num(); ++i)
	{
		chunks[i]->ShowCoordinates(visible);
	}
}