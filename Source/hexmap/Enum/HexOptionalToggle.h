// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HexOptionalToggle.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EHexOptionalToggle : uint8
{
	E_Ignore UMETA(DisplayName = "Ignore"),
	E_Yes UMETA(DisplayName = "Yes"),
	E_No UMETA(DisplayName = "No")
};