// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HexEdgeType.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EHexEdgeType : uint8
{
	E_Flat UMETA(DisplayName = "Flat"),
	E_Slope UMETA(DisplayName = "Slope"),
	E_Cliff UMETA(DisplayName = "Cliff")
};

namespace HexEdgeType {
	constexpr std::initializer_list<EHexEdgeType> all_edgeTypes = { EHexEdgeType::E_Flat, EHexEdgeType::E_Slope, EHexEdgeType::E_Cliff };

	static EHexEdgeType GetEdgeType(int elevation1, int elevation2)
	{
		if (elevation1 == elevation2)
		{
			return EHexEdgeType::E_Flat;
		}
		int delta = elevation2 - elevation1;
		if (delta == 1 || delta == -1)
		{
			return EHexEdgeType::E_Slope;
		}
		return EHexEdgeType::E_Cliff;
	}
}