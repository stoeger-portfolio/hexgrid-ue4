// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HexDirection.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EHexDirection : uint8
{
	E_NE UMETA(DisplayName = "NorthEast"),
	E_E UMETA(DisplayName = "East"),
	E_SE UMETA(DisplayName = "SouthEast"),
	E_SW UMETA(DisplayName = "SouthWest"),
	E_W UMETA(DisplayName = "West"),
	E_NW UMETA(DisplayName = "NorthWest")
};

namespace HexDirection {
	constexpr std::initializer_list<EHexDirection> all_directions = { EHexDirection::E_NE, EHexDirection::E_E, EHexDirection::E_SE, EHexDirection::E_SW, EHexDirection::E_W, EHexDirection::E_NW };

	static EHexDirection Opposite(EHexDirection direction)
	{
		return static_cast<EHexDirection>((int)direction < 3 ? ((int)direction + 3) : ((int)direction - 3));
	}
	static EHexDirection Previous(EHexDirection direction)
	{
		return static_cast<EHexDirection>(direction == EHexDirection::E_NE ? (int)EHexDirection::E_NW : ((int)direction - 1));
	}
	static EHexDirection Next(EHexDirection direction)
	{
		return static_cast<EHexDirection>(direction == EHexDirection::E_NW ? (int)EHexDirection::E_NE : ((int)direction + 1));
	}
}