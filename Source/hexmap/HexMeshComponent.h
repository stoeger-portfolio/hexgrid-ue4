// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ConstructorHelpers.h"
#include "ProceduralMeshComponent.h"
#include "Enum/HexDirection.h"
#include "HexCell.h"
#include "hexmetrics.h"
#include "HexMeshComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HEXMAP_API UHexMeshComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHexMeshComponent();
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* MeshComponent;
	
	UFUNCTION()
	void Triangulate(TArray<AHexCell*> cells);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY()
	UMaterialInterface* material;

	void Triangulate(const AHexCell* cell);
	void Triangulate(EHexDirection direction, const AHexCell* cell);
	void TriangulateConnection(EHexDirection direction, const AHexCell *cell, FVector v1, FVector v2);
	void TriangulateCorner(FVector bottom, FVector left, FVector right, const AHexCell* bottomCell, const AHexCell* leftCell, const AHexCell* rightCell);

	void TriangulateEdgeTerraces(FVector beginLeft, FVector beginRight, const AHexCell* beginCell, FVector endLeft, FVector endRight, const AHexCell* endCell);
	void TriangulateCornerTerraces(FVector begin, FVector left, FVector right, const AHexCell* beginCell, const AHexCell* leftCell, const AHexCell* rightCell);
	void TriangulateCornerTerracesCliff(FVector begin, FVector left, FVector right, const AHexCell* beginCell, const AHexCell* leftCell, const AHexCell* rightCell);
	void TriangulateCornerCliffTerraces(FVector begin, FVector left, FVector right, const AHexCell* beginCell, const AHexCell* leftCell, const AHexCell* rightCell);
	void TriangulateBoundaryTriangle(FVector begin, FVector left, FVector boundary, const AHexCell* beginCell, const AHexCell* leftCell, FLinearColor boundaryColor);

	void AddQuadMeta(FVector v1, FVector v2, FVector v3, FVector v4);
	void AddQuad(FVector v1, FVector v2, FVector v3, FVector v4, FLinearColor c1, FLinearColor c2);
	void AddQuad(FVector v1, FVector v2, FVector v3, FVector v4, FLinearColor c1, FLinearColor c2, FLinearColor c3, FLinearColor c4);

	void AddTriangle(FVector v1, FVector v2, FVector v3, FLinearColor color);
	void AddTriangle(FVector v1, FVector v2, FVector v3, FLinearColor c1, FLinearColor c2, FLinearColor c3);
	void AddTriangleMeta(FVector v1, FVector v2, FVector v3);

	TArray<FColor> colors;
};
