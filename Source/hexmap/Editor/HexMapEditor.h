// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HexCell.h"
#include "HexGrid.h"
#include "Enum/HexOptionalToggle.h"
#include "HexMapEditor.generated.h"

UCLASS()
class HEXMAP_API AHexMapEditor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHexMapEditor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	FLinearColor activeColor;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	int activeElevation;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	int brushSize;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	bool bApplyColor;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	bool bApplyElevation = true;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	EHexOptionalToggle riverMode;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	bool bIsDrag;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	EHexDirection dragDirection;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	AHexCell *previousCell;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Hex")
	AHexGrid *hexGrid;
		
	UFUNCTION(BlueprintCallable)
	void EditCell(AHexCell* cell);

	UFUNCTION(BlueprintCallable)
	void ShowCoordinates(bool visible);

	UFUNCTION(BlueprintCallable)
	void EditCells(const AHexCell* center);

private:
	void ValidateDrag(const AHexCell* currentCell);
};
