// Fill out your copyright notice in the Description page of Project Settings.


#include "HexMapEditor.h"

// Sets default values
AHexMapEditor::AHexMapEditor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHexMapEditor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHexMapEditor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHexMapEditor::ValidateDrag(const AHexCell* currentCell)
{
	for (EHexDirection d : HexDirection::all_directions)
	{
		dragDirection = d;
		if (previousCell->GetNeighbor(dragDirection) == currentCell)
		{
			bIsDrag = true;
			return;
		}
	}
	bIsDrag = false;
}

void AHexMapEditor::EditCells(const AHexCell* center)
{
	if (previousCell != nullptr && previousCell != center)
	{
		ValidateDrag(center);
	}
	else {
		bIsDrag = false;
	}
	int centerX = center->coordinates.X();
	int centerZ = center->coordinates.Z();

	for (int r = 0, z = centerZ - brushSize; z <= centerZ; ++z, ++r)
	{
		for (int x = centerX - r; x <= centerX + brushSize; x++) {
			EditCell(hexGrid->GetCellByCoordinates(x, z));
		}
	}

	for (int r = 0, z = centerZ + brushSize; z > centerZ; z--, r++) {
		for (int x = centerX - brushSize; x <= centerX + r; x++) {
			EditCell(hexGrid->GetCellByCoordinates(x, z));
		}
	}
}

void AHexMapEditor::EditCell(AHexCell* cell)
{
	if (cell != nullptr) {
		if (bApplyColor)
		{
			cell->SetColor(activeColor);
		}
		if (bApplyElevation)
		{
			cell->SetElevation(activeElevation);
		}
		if (riverMode == EHexOptionalToggle::E_No)
		{
			cell->RemoveRiver();
		}
		else if (riverMode == EHexOptionalToggle::E_Yes)
		{
			AHexCell* otherCell = cell->GetNeighbor(HexDirection::Opposite(dragDirection));
			if (otherCell != nullptr) {
				otherCell->SetOutGoingRiver(dragDirection);
			}
		}
	}
}

void AHexMapEditor::ShowCoordinates(bool visible)
{
	hexGrid->ShowCoordinates(visible);
}